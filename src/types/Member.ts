type Member = {
  id: number
  name: string
  tel: string
  point: number
  accumulate: number
  discount: number
}

export type { Member }
