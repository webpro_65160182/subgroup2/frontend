import type { Item } from './Item'
import type { Member } from './Member'
import type { User } from './User'
type paymentType = 'Cash' | 'QR'
type Receipt = {
  id: number
  createdDate: Date
  total: number
  totalNet: number
  memberDiscount: number
  receivedAmount: number
  change: number
  paymentType: string
  userId: number
  user: string
  memberId: number
  member?: Member
  itemLists?: Item[]
}
export type { Receipt, paymentType }
