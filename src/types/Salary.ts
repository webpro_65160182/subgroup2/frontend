type Salary = {
  id: number
  empId: number
  name: string
  pay: number
  date: string
  workHour: number
}

export type { Salary }
