type Gender = 'Male' | 'Female' | 'Others'
type Roles = 'Manager' | 'Staff'
type User = {
  id: number
  email: string
  password: string
  name: string
  gender: Gender
  roles: Roles[]
}

export type { Gender, Roles, User }
