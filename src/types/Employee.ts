type Status = 'PartTime' | 'FullTime'
type Employee = {
  id: number
  name: string
  tel: string
  status: Status[] //PartTime,FullTime
  salary: number
  dateIn: string
  checkIn: string
  checkOut: string
}

export type { Status, Employee }
