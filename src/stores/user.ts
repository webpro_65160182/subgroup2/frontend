import type { User } from '@/types/User'
import { defineStore } from 'pinia'
import { ref } from 'vue'
import { useRouter } from 'vue-router'

export const useUserStore = defineStore('user', () => {
  const users = ref<User[]>([
    {
      id: 1,
      email: 'Paweena@gmail.com',
      password: 'Paweena_1234',
      name: 'Paweena Chinasri',
      gender: 'Female',
      roles: ['Manager']
    },
    {
      id: 2,
      email: 'Warissara@gmail.com',
      password: 'Warissara_1234',
      name: 'Warissara Kanjana',
      gender: 'Female',
      roles: ['Staff']
    },
    {
      id: 3,
      email: 'Pitchayaporn@gmail.com',
      password: 'Pitchayaporn_1234',
      name: 'Pitchayaporn Tawornwong',
      gender: 'Female',
      roles: ['Staff']
    },
    {
      id: 4,
      email: 'Phattharapron@gmail.com',
      password: 'Phattharapron_1234',
      name: 'Phattharapron Tancharoen',
      gender: 'Female',
      roles: ['Staff']
    }
  ])

  const username = ref('')
  const password = ref('')
  const router = useRouter()
  const dialogFailed = ref(false)
  function login() {
    const foundUser = users.value.find(
      (item) => item.email === username.value && item.password === password.value
    )
    if (foundUser) {
      router.push('/pos')
      useUserStore().setLoggedInUser(foundUser.name, foundUser.email)
    } else {
      dialogFailed.value = true
      return
    }
  }

  const loggedInUser = ref<{ name: string; email: string } | null>(null)
  function setLoggedInUser(name: string, email: string) {
    loggedInUser.value = { name, email }
  }

  function closeDialog() {
    dialogFailed.value = false
    clear()
  }
  function clear() {
    username.value = ''
    password.value = ''
    setLoggedInUser('', '')
  }
  function logout() {
    clear()
    router.push('/login')
  }

  return {
    users,
    username,
    password,
    loggedInUser,
    dialogFailed,
    login,
    setLoggedInUser,
    closeDialog,
    logout
  }
})
