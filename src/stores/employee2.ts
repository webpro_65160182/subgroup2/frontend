import type { Employee } from '@/types/Employee'
import { defineStore } from 'pinia'
import { ref } from 'vue'
import { useLoadingStore } from './loading'
import employeeService from '@/services/employee'


export const useEmployStore2 = defineStore('employee', () => {
    const loadingStore = useLoadingStore()

    const Employees = ref<Employee[]>([])
    //กำหนดหัวData table

    //Data Function
    async function getEmployee(id: number) {
        loadingStore.doLoad()
        const res = await employeeService.getEmployee(id)
        Employees.value = res.data
        loadingStore.finish()
        //มีปัญหาการเรียกใช้
    }

    async function getEmployees() {
        console.log("เรียกใช้่")
        loadingStore.doLoad()
        const res = await employeeService.getEmployees()
        Employees.value = res.data
        loadingStore.finish()
        console.log("เรียกใช้เสร็จ")
        //มีปัญหาการเรียกใช้
    }

    async function saveEmployee(employee: Employee) {
        loadingStore.doLoad()
        if (employee.id < 0) { //Add New
            const res = await employeeService.addEmployee(employee)
        } else { //Update
            const res = await employeeService.updateEmployee(employee)
        }
        await getEmployees()
        loadingStore.finish()
    }

    async function deleteEmployee(employee: Employee) {
        loadingStore.doLoad()
        const res = await employeeService.delEmployee(employee)
        await getEmployees()
        loadingStore.finish()
    }

    //End Data Function

    return {
        Employees,
        saveEmployee,
        deleteEmployee,
        getEmployees,
        getEmployee
    }
})