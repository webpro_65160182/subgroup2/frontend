import type { Employee } from '@/types/Employee'
import { defineStore } from 'pinia'
import { nextTick, ref } from 'vue'
import { onMounted } from 'vue'
import { useLoadingStore } from './loading'
import employeeService from '@/services/employee'


export const useEmployStore = defineStore('employee', () => {
  const loadingStore = useLoadingStore()

  const Employees = ref<Employee[]>([])
  //กำหนดหัวData table
  const headers = [
    {
      title: 'Employee ID',
      key: 'id',
      value: 'id'
    },
    {
      title: 'Employee Name',
      key: 'name',
      value: 'name'
    },
    {
      title: 'Tel',
      key: 'tel',
      value: 'tel'
    },
    {
      title: 'Status',
      key: 'status',
      value: (item: any | Employee) => item.status.join(', ')
    },
    {
      title: 'Check In',
      key: 'checkIn',
      value: 'checkIn'
    },
    {
      title: 'Check Out',
      key: 'checkOut',
      value: 'checkOut'
    },
    {
      title: 'Slary',
      key: 'salary',
      value: 'salary'
    },
    { title: '', key: 'actions', sortable: false }
  ]
  const form = ref(false)

  const dialog = ref(false)
  const dialogDelete = ref(false)
  const loading = ref(false)
  const initilEmployee: Employee = {
    id: -1,
    name: '',
    tel: '',
    status: ['PartTime'],
    salary: 0,
    dateIn: '',
    checkIn: '',
    checkOut: ''
  }

  const editedEmployee = ref<Employee>(JSON.parse(JSON.stringify(initilEmployee)))

  let editedIndex = -1
  let lastId = 5
  function onSubmit() { }

  function closeDelete() {
    dialogDelete.value = false
    nextTick(() => {
      editedEmployee.value = Object.assign({}, initilEmployee)
    })
  }
  function deleteItemConfirm() {
    Employees.value.splice(editedIndex, 1)
    closeDelete()
  }
  function editItem(item: Employee) {
    editedIndex = Employees.value.indexOf(item)
    editedEmployee.value = Object.assign({}, item)
    dialog.value = true
  }
  function deleteItem(item: Employee) {
    editedIndex = Employees.value.indexOf(item)
    editedEmployee.value = Object.assign({}, item)
    dialogDelete.value = true
    editedIndex = -1
    const index = Employees.value.findIndex((Employee) => Employee.id === item.id)
    if (index !== -1) {
      Employees.value.splice(index, 1)
    }
    closeDelete()
  }
  function closeDialog() {
    dialog.value = false
    nextTick(() => {
      editedEmployee.value = Object.assign({}, initilEmployee)
      editedIndex = -1
    })
  }
  async function save() {
    if (editedIndex > -1) {
      Object.assign(Employees.value[editedIndex], editedEmployee.value)
    } else {
      editedEmployee.value.id = lastId++
      Employees.value.push(editedEmployee.value)
    }
    closeDialog()
  }
  function initialize() {
    Employees.value = [
      {
        id: 1,
        name: 'Mod Dang',
        tel: '0811651848',
        status: ['PartTime', 'FullTime'],
        salary: 1000,
        dateIn: '2022-01-08',
        checkIn: '08.00.55',
        checkOut: '16.00.48'
      },
      {
        id: 2,
        name: 'Ja Ja',
        tel: '0814132218',
        status: ['PartTime'],
        salary: 400,
        dateIn: '2022-01-08',
        checkIn: '08.00.55',
        checkOut: '16.00.48'
      },
      {
        id: 3,
        name: 'Mind Grian',
        tel: '0815184623',
        status: ['PartTime'],
        salary: 400,
        dateIn: '2022-01-08',
        checkIn: '08.00.55',
        checkOut: '16.00.48'
      },
      {
        id: 4,
        name: 'Poy Fon',
        tel: '0814745158',
        status: ['PartTime'],
        salary: 400,
        dateIn: '2022-01-08',
        checkIn: '08.00.55',
        checkOut: '16.00.48'
      }
    ]
  }

  onMounted(() => {
    initialize()
  })

  function searchfor(Input: string) {
    Employees.value = Employees.value.filter(function (Employees) {
      return (
        Employees.name.toLowerCase().includes(Input.toLowerCase()) || Employees.tel.includes(Input)
      )
    })
  }

  //Data Function
  async function getEmployees() {
    console.log("เรียกใช้่")
    loadingStore.doLoad()
    const res = await employeeService.getEmployees()
    Employees.value = res.data
    loadingStore.finish()
    console.log("เรียกใช้เสร็จ")
    //มีปัญหาการเรียกใช้
  }

  async function saveEmployee(employee: Employee) {
    loadingStore.doLoad()
    if (employee.id < 0) { //Add New
      const res = await employeeService.addEmployee(employee)
    } else { //Update
      const res = await employeeService.updateEmployee(employee)
    }
    await getEmployees()
    loadingStore.finish()
  }

  async function deleteEmployee(employee: Employee) {
    loadingStore.doLoad()
    const res = await employeeService.delEmployee(employee)
    await getEmployees()
    loadingStore.finish()
  }

  //End Data Function

  return {
    headers,
    form,
    dialog,
    dialogDelete,
    loading,
    initilEmployee,
    editedEmployee,
    Employees,
    editedIndex,
    lastId,
    onSubmit,
    closeDelete,
    deleteItemConfirm,
    editItem,
    deleteItem,
    closeDialog,
    save,
    initialize,
    onMounted,
    searchfor,
    saveEmployee,
    deleteEmployee,
    getEmployees
  }
})
