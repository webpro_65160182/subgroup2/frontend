import { ref } from 'vue'
import { defineStore } from 'pinia'
import type { Item } from '@/types/Item'
import type { Product } from '@/types/Product'
import type { Receipt } from '@/types/Receipt'
import { useMemberStore } from './member'

export const userItemStore = defineStore('item', () => {
  const memberStore = useMemberStore()
  const receiptDialog = ref(false)
  const receipt = ref<Receipt>({
    id: 0,
    createdDate: new Date(),
    total: 0,
    totalNet: 0,
    memberDiscount: 0,
    receivedAmount: 0,
    change: 0,
    paymentType: 'cash',
    userId: 0,
    user: '',
    memberId: 0
  })
  const itemLists = ref<Item[]>([])
  function addItem(product: Product) {
    const index = itemLists.value.findIndex((item) => item.product.id === product.id)
    if (index >= 0) {
      itemLists.value[index].unit++
      calculate()
      return
    } else {
      const newItem: Item = {
        id: -1,
        name: product.name,
        unit: 1,
        price: product.price,
        productId: product.id,
        product: product
      }
      itemLists.value.push(newItem)
      calculate()
    }
  }
  function removeItem(itemList: Item) {
    const index = itemLists.value.findIndex((item) => item === itemList)
    itemLists.value.splice(index, 1)
    calculate()
  }
  function inc(item: Item) {
    item.unit++
    calculate()
  }
  function deinc(item: Item) {
    if (item.unit === 1) {
      removeItem(item)
    }
    item.unit--
    calculate()
  }

  function calculate() {
    let total = 0
    let memberDiscount = 0
    for (const item of itemLists.value) {
      total = total + item.price * item.unit
    }
    receipt.value.total = total
    if (memberStore.currentMember) {
      memberDiscount = total - total * 0.95
      receipt.value.memberDiscount = parseFloat(memberDiscount.toFixed(2))
      receipt.value.totalNet = parseFloat((total * 0.95).toFixed(2))
    } else {
      receipt.value.totalNet = parseFloat(total.toFixed(2))
    }
  }

  function billDialog() {
    receipt.value.itemLists = itemLists.value
    receiptDialog.value = true
  }

  function clear() {
    itemLists.value = []
    receipt.value = {
      id: 0,
      createdDate: new Date(),
      total: 0,
      totalNet: 0,
      memberDiscount: 0,
      receivedAmount: 0,
      change: 0,
      paymentType: 'cash',
      userId: 0,
      user: '',
      memberId: 0
    }
    memberStore.clear()
  }

  return {
    itemLists,
    receipt,
    receiptDialog,
    addItem,
    removeItem,
    inc,
    deinc,
    calculate,
    billDialog,
    clear
  }
})
