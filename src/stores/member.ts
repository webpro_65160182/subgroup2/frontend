import { nextTick, onMounted, ref } from 'vue'
import { defineStore } from 'pinia'
import type { Member } from '@/types/Member'
import type { VForm } from 'vuetify/components'

export const useMemberStore = defineStore('member', () => {
  const members = ref<Member[]>([])
  const tel = ref('')
  const memberstore = useMemberStore()
  function onClick() {
    memberstore.searchMember(tel.value)
  }
  const headers = [
    {
      title: 'Id',
      key: 'id',
      value: 'id'
    },
    {
      title: 'Name',
      key: 'name',
      value: 'name'
    },
    {
      title: 'Tel',
      key: 'tel',
      value: 'tel'
    },
    {
      title: 'Point',
      key: 'point',
      value: 'point'
    },
    {
      title: 'Accumulate',
      key: 'accumulate',
      value: 'accumulate'
    },
    {
      title: 'Discount',
      key: 'discount',
      value: 'discount'
    },
    { title: 'Actions', key: 'actions', sortable: false }
  ]
  const form = ref(false)
  const refForm = ref<VForm | null>(null)
  const dialog = ref(false)
  const loading = ref(false)
  const dialogDelete = ref(false)
  const initilMember: Member = {
    id: -1,
    name: '',
    tel: '',
    point: 0,
    accumulate: 0,
    discount: 0
  }
  const editedMember = ref<Member>(JSON.parse(JSON.stringify(initilMember)))
  let editedIndex = -1
  let lastId = 4
  let lastPoint = 1
  function onSubmit() {}

  function closeDelete() {
    dialogDelete.value = false
    nextTick(() => {
      editedMember.value = Object.assign({}, initilMember)
    })
  }
  function deleteItemConfirm() {
    // Delete item from list
    members.value.splice(editedIndex, 1)
    closeDelete()
  }
  function editItem(item: Member) {
    editedIndex = members.value.indexOf(item)
    editedMember.value = Object.assign({}, item)
    dialog.value = true
  }
  function deleteItem(item: Member) {
    editedIndex = members.value.indexOf(item)
    editedMember.value = Object.assign({}, item)
    dialogDelete.value = true
    editedIndex = -1
  }
  function closeDialog() {
    dialog.value = false
    nextTick(() => {
      editedMember.value = Object.assign({}, initilMember)
      editedIndex = -1
    })
  }
  async function save() {
    const { valid } = await refForm.value!.validate()
    if (!valid) return

    if (editedIndex > -1) {
      Object.assign(members.value[editedIndex], editedMember.value)
    } else {
      editedMember.value.id = lastId++
      editedMember.value.point = lastPoint++
      members.value.push(editedMember.value)
    }

    editedIndex = -1
    closeDialog()
  }
  function initialize() {
    members.value = [
      {
        id: 1,
        name: 'Mod Mod',
        tel: '0812345678',
        point: 50,
        accumulate: 60,
        discount: 10
      },
      {
        id: 2,
        name: 'Ja Ja',
        tel: '0823456789',
        point: 48,
        accumulate: 50,
        discount: 5
      },
      {
        id: 3,
        name: 'Poy Poy',
        tel: '0834567891',
        point: 29,
        accumulate: 30,
        discount: 0
      },
      {
        id: 4,
        name: 'Mild Mild',
        tel: '0845678912',
        point: 55,
        accumulate: 55,
        discount: 10
      },
      {
        id: 5,
        name: 'Ha Ha',
        tel: '0856789123',
        point: 99,
        accumulate: 100,
        discount: 20
      }
    ]
  }

  onMounted(() => {
    initialize()
  })
  const currentMember = ref<Member | null>()
  const searchMember = (tel: string) => {
    const index = members.value.findIndex((item) => item.tel === tel)
    if (index < 0) {
      currentMember.value = null
    }
    currentMember.value = members.value[index]
  }
  function clear() {
    currentMember.value = null
  }
  return {
    members,
    currentMember,
    dialog,
    dialogDelete,
    editedIndex,
    editedMember,
    form,
    headers,
    initilMember,
    clear,
    closeDelete,
    closeDialog,
    deleteItem,
    deleteItemConfirm,
    editItem,
    save,
    searchMember,
    onSubmit,
    onClick,
    onMounted,
    initialize
  }
})
