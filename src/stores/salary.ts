import type { Salary } from '@/types/Salary'
import { defineStore } from 'pinia'
import { nextTick, ref } from 'vue'
import { onMounted } from 'vue'
import SalaryDialogVue from '@/components/SalaryDialog.vue'

export const SalaryStore = defineStore('salary', () => {
  const Salarys = ref<Salary[]>([])
  //กำหนดหัวData table
  const headers = [
    {
      title: 'Salary ID',
      key: 'id',
      value: 'id'
    },
    {
      title: 'Employee ID',
      key: 'empId',
      value: 'empId'
    },
    {
      title: 'Employee Name',
      key: 'name',
      value: 'name'
    },
    {
      title: 'Pay',
      key: 'pay',
      value: 'pay'
    },
    {
      title: 'Date',
      key: 'date',
      value: 'date'
    },
    {
      title: 'Work Hours',
      key: 'workHour',
      value: 'workHour'
    },
    { title: '', key: 'actions', sortable: false }
  ]
  const form = ref(false)

  const dialog = ref(false)
  const dialogDelete = ref(false)
  const loading = ref(false)
  const initilSalary: Salary = {
    id: -1,
    empId: 0,
    name: '',
    pay: 0,
    date: '',
    workHour: 0
  }
  const editedSalary = ref<Salary>(JSON.parse(JSON.stringify(initilSalary)))
  // ข้อมูล

  let editedIndex = -1
  let lastId = 5
  function onSubmit() {}

  function closeDelete() {
    dialogDelete.value = false
    nextTick(() => {
      editedSalary.value = Object.assign({}, initilSalary)
    })
  }
  function deleteItemConfirm() {
    Salarys.value.splice(editedIndex, 1)
    closeDelete()
  }
  function editItem(item: Salary) {
    editedIndex = Salarys.value.indexOf(item)
    editedSalary.value = Object.assign({}, item)
    dialog.value = true
  }
  function deleteItem(item: Salary) {
    editedIndex = Salarys.value.indexOf(item)
    editedSalary.value = Object.assign({}, item)
    dialogDelete.value = true
    editedIndex = -1
    const index = Salarys.value.findIndex((Salary) => Salary.id === item.id)
    if (index !== -1) {
      Salarys.value.splice(index, 1)
    }
    closeDelete()
  }
  function closeDialog() {
    dialog.value = false
    nextTick(() => {
      editedSalary.value = Object.assign({}, initilSalary)
      editedIndex = -1
    })
  }
  async function save() {
    if (editedIndex > -1) {
      Object.assign(Salarys.value[editedIndex], editedSalary.value)
    } else {
      editedSalary.value.id = lastId++
      Salarys.value.push(editedSalary.value)
    }
    closeDialog()
  }
  function initializeSalary() {
    Salarys.value = [
      {
        id: 1,
        empId: 1,
        name: 'Mod Dang',
        pay: 1000,
        date: '2022-01-08',
        workHour: 1
      },
      {
        id: 2,
        empId: 2,
        name: 'Ja Ja',
        pay: 400,
        date: '2022-01-08',
        workHour: 1
      },
      {
        id: 3,
        empId: 3,
        name: 'Mind Grian',
        pay: 400,
        date: '2022-01-08',
        workHour: 1
      },
      {
        id: 4,
        empId: 4,
        name: 'Poy Fon',
        pay: 400,
        date: '2022-01-08',
        workHour: 1
      }
    ]
  }

  function openDialog() {}
  onMounted(() => {
    initializeSalary()
  })

  function searchfor(Input: string) {
    Salarys.value = Salarys.value.filter(function (Salarys) {
      return (
        Salarys.name.toLowerCase().includes(Input.toLowerCase()) || Salarys.empId == parseInt(Input)
      )
    })
  }
  return {
    headers,
    form,
    dialog,
    dialogDelete,
    loading,
    initilSalary,
    editedSalary,
    Salarys,
    editedIndex,
    lastId,
    onSubmit,
    closeDelete,
    deleteItemConfirm,
    editItem,
    deleteItem,
    closeDialog,
    save,
    initializeSalary,
    onMounted,
    searchfor
  }
})
